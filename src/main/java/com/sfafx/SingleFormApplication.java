package com.sfafx;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Single Form Application FX Framework
 *
 * @version 1.3.2
 * @author Alexey Potopakhin
 */
public abstract class SingleFormApplication extends Application {
    private static SingleFormApplication singleFormApplication;

    private Stage primaryStage;
    private Scene scene;
    private Map<String, Parent> views = new HashMap<>();
    private Map<String, Object> controllers = new HashMap<>();

    private String currentView = null;

    @Override
    public void start(Stage primaryStage) {
        singleFormApplication = this;

        this.primaryStage = primaryStage;
        initialize();

        // Setting default parent view
        Parent defaultViewParent;
        if(currentView != null) {
            defaultViewParent = views.get(currentView);
        } else {
            defaultViewParent = (Parent) views.values().toArray()[0];
        }

        this.scene = new Scene(defaultViewParent);
        primaryStage.setScene(this.scene);
        primaryStage.show();

        postInitialize();
    }

    /**
     * This method executes before application starts
     */
    public abstract void initialize();

    /**
     * This method executes after application starts
     */
    public void postInitialize() { }

    /**
     * Sets default view to show first after application starts
     *
     * @param viewName - view name
     */
    public void setDefaultView(String viewName) {
        this.currentView = viewName;
    }

    /**
     * Adds initialized view parent to the application
     *
     * @param viewName - view name
     * @param view - root node
     */
    public void addView(String viewName, Parent view) {
        this.views.put(viewName, view);
    }

    /**
     * Adds SFA FX View to the application
     *
     * @param view - SFA FX View
     */
    public void addView(View view) {
        Parent parent = null;
        try {
            parent = view.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        addView(view.getViewName(), parent);
        addController(view.getViewName(), view.getController());
    }

    /**
     * Removes view parent from the application
     *
     * @param viewName - view name
     */
    public void removeView(String viewName) {
        this.views.remove(viewName);
        this.controllers.remove(viewName);
    }

    public void setTitle(String title) {
        this.primaryStage.setTitle(title);
    }

    public void setWindowHeight(double height) {
        this.primaryStage.setHeight(height);
    }

    public void setWindowWidth(double width) {
        this.primaryStage.setWidth(width);
    }

    public void setIcon(String iconPath) {
        this.primaryStage.getIcons().add(new Image(iconPath));
    }

    public void setIcon(InputStream inputStream) {
        this.primaryStage.getIcons().add(new Image(inputStream));
    }

    public void addCommonCSS(String cssPath) {
        this.scene.getStylesheets().add(cssPath);
    }

    public void setResizable(boolean isResizable) {
        this.primaryStage.setResizable(isResizable);
    }

    public Stage getStage() {
        return this.primaryStage;
    }

    public Scene getScene() {
        return this.scene;
    }

    public void switchView(String viewName) {
        this.scene.setRoot(views.get(viewName));
        this.currentView = viewName;
        initSwitchEvent();
    }

    public void switchViewAndRemoveCurrent(String viewName) {
        this.scene.setRoot(views.get(viewName));
        removeView(this.currentView);
        this.currentView = viewName;
        initSwitchEvent();
    }

    public void addController(String viewName, Object controller) {
        this.controllers.put(viewName, controller);
    }

    private void initSwitchEvent() {
        Object controller = this.controllers.get(this.currentView);

        if(controller instanceof Switchable) {
            ((Switchable) controller).switchEvent();
        }
    }

    public static SingleFormApplication getInstance() {
        return singleFormApplication;
    }
}
