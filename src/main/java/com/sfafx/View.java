package com.sfafx;

import javafx.fxml.FXMLLoader;

import java.net.URL;

public class View extends FXMLLoader {
    private String viewName;

    public View(String viewName) {
        super();
        this.viewName = viewName;
    }

    public View(String viewName, URL fxmlFileLocation) {
        this(viewName);
        super.setLocation(fxmlFileLocation);
    }

    public String getViewName() {
        return viewName;
    }
}
