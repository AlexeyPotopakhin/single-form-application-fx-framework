package com.sfafx.controllers;

import com.sfafx.SingleFormApplication;
import com.sfafx.Switchable;
import com.sfafx.TestApplication;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class TwoController implements Initializable, Switchable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void switchButtonAction() {
        SingleFormApplication.getInstance().switchView(TestApplication.VIEW_ONE);
    }

    @Override
    public void switchEvent() {
        System.out.println("Switched to View Two");
    }
}
