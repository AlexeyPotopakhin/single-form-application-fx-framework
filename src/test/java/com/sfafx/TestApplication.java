package com.sfafx;

public class TestApplication extends SingleFormApplication {
    public static final String VIEW_ONE = "View One";
    public static final String VIEW_TWO = "View Two";

    @Override
    public void initialize() {
        View viewOne = new View(VIEW_ONE, TestApplication.class.getResource("views/one.fxml"));
        View viewTwo = new View(VIEW_TWO, TestApplication.class.getResource("views/two.fxml"));

        addView(viewOne);
        addView(viewTwo);

        setDefaultView(VIEW_ONE);
        setTitle("SFA FX Example Application");
    }

    @Override
    public void postInitialize() {
        addCommonCSS(TestApplication.class.getResource("css/styles.css").toExternalForm());
    }
}
